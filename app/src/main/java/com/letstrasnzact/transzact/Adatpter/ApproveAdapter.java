package com.letstrasnzact.transzact.Adatpter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.letstrasnzact.transzact.R;
import com.letstrasnzact.transzact.mData.Approve;

import java.util.ArrayList;

/**
 * Created by hi_man_shoe on 11/25/16.
 */

public class ApproveAdapter   extends BaseAdapter {
    Context c;
    Button b;
    ArrayList<Approve> pojo;
    LayoutInflater inflater;

    public ApproveAdapter(Context c, ArrayList<Approve> pojo) {
        this.c = c;
        this.pojo = pojo;
    }

    @Override
    public int getCount() {
        return pojo.size();
    }

    @Override
    public Object getItem(int position) {
        return pojo.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(inflater==null){
            inflater=(LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        if(convertView==null){
            convertView=inflater.inflate(R.layout.approvelist,parent,false);

        }
        TextView type=(TextView)convertView.findViewById(R.id.type);
        TextView amt=(TextView)convertView.findViewById(R.id.amount);
        TextView supplier=(TextView)convertView.findViewById(R.id.supplier);
        Button b = (Button)convertView.findViewById(R.id.app);
        final String mtype=pojo.get(position).getType();
        final String mAmnt=pojo.get(position).getAmt();
        final String mSupplier=pojo.get(position).getSupplier();


        supplier.setText(mSupplier);
        type.setText(mtype);
        amt.setText(mAmnt);
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        c);

                // set title
                alertDialogBuilder.setTitle("Approval?");

                // set dialog message
                alertDialogBuilder
                        .setMessage("Confirm your status")
                        .setCancelable(false)
                        .setPositiveButton("Yes",new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // if this button is clicked, close
                                // current activity

                            }
                        })
                        .setNegativeButton("No",new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                // if this button is clicked, just close
                                // the dialog box and do nothing
                                dialog.cancel();
                            }
                        });

                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();

            }
        });

        return convertView;


}}


