package com.letstrasnzact.transzact.Adatpter;

import android.content.Context;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.letstrasnzact.transzact.R;
import com.letstrasnzact.transzact.mData.Pojo;

import java.util.ArrayList;

/**
 * Created by hi_man_shoe on 11/25/16.
 */

public class CustomAdapter extends BaseAdapter {
    Context c;
    ArrayList<Pojo> pojo;
    LayoutInflater inflater;

    public CustomAdapter(Context c, ArrayList<Pojo> pojo) {
        this.c = c;
        this.pojo = pojo;
    }

    @Override
    public int getCount() {
        return pojo.size();
    }

    @Override
    public Object getItem(int position) {
        return pojo.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(inflater==null){
            inflater=(LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        if(convertView==null){
            convertView=inflater.inflate(R.layout.list,parent,false);

        }
        TextView nameTxt=(TextView)convertView.findViewById(R.id.txt);
        ImageView img=(ImageView)convertView.findViewById(R.id.img);

        final String name=pojo.get(position).getName();
        int image=pojo.get(position).getImage();

        nameTxt.setText(name);
        img.setImageResource(image);

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(c,name,Toast.LENGTH_SHORT).show();
            }
        });

        return convertView;
    }
}
