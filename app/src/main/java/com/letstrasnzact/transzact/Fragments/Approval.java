package com.letstrasnzact.transzact.Fragments;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;

import com.letstrasnzact.transzact.Adatpter.ApproveAdapter;
import com.letstrasnzact.transzact.Adatpter.CustomAdapter;
import com.letstrasnzact.transzact.R;
import com.letstrasnzact.transzact.mData.Approve;
import com.letstrasnzact.transzact.mData.Pojo;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class Approval extends Fragment {
    @Nullable
    Button accept;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container , Bundle savedInstanceState){

        View rootView=inflater.inflate(R.layout.fragment_approval,container,false) ;


        ListView lv=(ListView)rootView.findViewById(R.id.listView);
        ApproveAdapter adapter=new ApproveAdapter(this.getActivity(),getNotification());
        lv.setAdapter(adapter);
        return rootView;
    }
    private ArrayList<Approve> getNotification(){
        //Collection of Notification
        ArrayList<Approve> notification= new ArrayList<>();
        //single notification
        Approve notifications = new Approve("Type: Purchase Order","Amount: Rs 1000000","Supplier: Honet Supplier");
        notification.add(notifications);

        notifications = new Approve("Type: Invoice","Amount: Rs 1000000","Supplier: Honet Supplier");
        notification.add(notifications);

        notifications = new Approve("Type: Indent","Amount: Rs 1000000","Supplier: Honet Supplier");
        notification.add(notifications);



        return  notification;
    }

}