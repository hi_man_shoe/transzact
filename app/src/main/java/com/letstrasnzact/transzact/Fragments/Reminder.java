package com.letstrasnzact.transzact.Fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.letstrasnzact.transzact.Adatpter.CustomAdapter;
import com.letstrasnzact.transzact.R;
import com.letstrasnzact.transzact.mData.Pojo;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class Reminder extends Fragment {


    public Reminder() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_reminder, container, false);
    }

}
