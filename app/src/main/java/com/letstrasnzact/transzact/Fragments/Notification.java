package com.letstrasnzact.transzact.Fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.letstrasnzact.transzact.Adatpter.CustomAdapter;
import com.letstrasnzact.transzact.R;
import com.letstrasnzact.transzact.mData.Pojo;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class Notification extends Fragment {

   @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container , Bundle savedInstanceState){

       View rootView=inflater.inflate(R.layout.fragment_notification,container,false) ;

       ListView lv=(ListView)rootView.findViewById(R.id.list);
       CustomAdapter adapter=new CustomAdapter(this.getActivity(),getNotification());
       lv.setAdapter(adapter);
       return rootView;
   }
    private ArrayList<Pojo> getNotification(){
        //Collection of Notification
        ArrayList<Pojo> notification= new ArrayList<>();
        //single notification
        Pojo notifications = new Pojo("Suresh sent an Invoice of Rs 1,00,000 to be paid by 25 Nov 2016",R.drawable.dell);
        notification.add(notifications);

        notifications = new Pojo(" Greenspan Tech sent a Purchase Order of Rs 3,00,000 to be delivered by 20 Oct 2016\n",R.drawable.green);
        notification.add(notifications);

        notifications = new Pojo("Suresh sent an Invoice of Rs 1,00,000 to be paid by 25 Nov 2016",R.drawable.dell);
        notification.add(notifications);

        notifications = new Pojo(" Greenspan Tech sent a Purchase Order of Rs 3,00,000 to be delivered by 20 Oct 2016\n",R.drawable.green);
        notification.add(notifications);
        notifications = new Pojo("Suresh sent an Invoice of Rs 1,00,000 to be paid by 25 Nov 2016",R.drawable.dell);
        notification.add(notifications);

        notifications = new Pojo(" Greenspan Tech sent a Purchase Order of Rs 3,00,000 to be delivered by 20 Oct 2016\n",R.drawable.green);
        notification.add(notifications);
        notifications = new Pojo("Suresh sent an Invoice of Rs 1,00,000 to be paid by 25 Nov 2016",R.drawable.dell);
        notification.add(notifications);

        notifications = new Pojo(" Greenspan Tech sent a Purchase Order of Rs 3,00,000 to be delivered by 20 Oct 2016\n",R.drawable.green);
        notification.add(notifications);
        notifications = new Pojo("Suresh sent an Invoice of Rs 1,00,000 to be paid by 25 Nov 2016",R.drawable.dell);
        notification.add(notifications);

        notifications = new Pojo(" Greenspan Tech sent a Purchase Order of Rs 3,00,000 to be delivered by 20 Oct 2016\n",R.drawable.green);
        notification.add(notifications);

return  notification;
    }

}