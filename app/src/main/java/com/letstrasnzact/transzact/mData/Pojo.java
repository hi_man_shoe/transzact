package com.letstrasnzact.transzact.mData;

/**
 * Created by hi_man_shoe on 11/25/16.
 */

public class Pojo {


    String name;
    int image;

    public Pojo(String name, int image) {
        this.name = name;
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public int getImage() {
        return image;
    }
}

