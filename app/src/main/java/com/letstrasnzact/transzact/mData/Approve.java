package com.letstrasnzact.transzact.mData;

/**
 * Created by hi_man_shoe on 11/25/16.
 */

public class Approve {
    String type;
    String amt;
    String supplier;
    public Approve(String name, String amt,String supplier ) {
        this.type = name;
        this.amt = amt;
        this.supplier = supplier;

    }

    public String getSupplier() {
        return supplier;
    }

    public String getAmt() {
        return amt;
    }

    public String getType() {
        return type;
    }


}
